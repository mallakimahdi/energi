FROM debian:10

ENV ENG_VERSION=v3.1.3

WORKDIR /app

ADD https://s3-us-west-2.amazonaws.com/download.energi.software/releases/energi3/v3.1.3/energi3-$ENG_VERSION-linux-amd64.tgz /app/

RUN tar -xvzf *.tgz && rm -f *.tgz

RUN useradd user1 && groupadd group1 && mkdir -p /home/user1 && chown -R user1:group1 /home/user1
USER user1:group1

CMD energi3-$ENG_VERSION-linux-amd64/bin/energi3
